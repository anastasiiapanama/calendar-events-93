import React from 'react';
import {Switch, Route} from "react-router-dom";

import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Events from "./containers/Events/Events";
import NewEvents from "./containers/NewEvents/NewEvents";

const App = () => (
    <>
        <CssBaseline/>
        <header>
            <AppToolbar/>
        </header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={Events}/>
                    <Route path="/events/new" exact component={NewEvents}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
