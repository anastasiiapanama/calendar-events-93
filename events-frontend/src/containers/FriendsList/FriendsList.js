import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {Card, CardActions, CardContent, IconButton, makeStyles, Typography} from "@material-ui/core";
import BackspaceIcon from "@material-ui/icons/Backspace";
import {useDispatch, useSelector} from "react-redux";
import {inviteDeleteRequest, invitesRequest} from "../../store/actions/invitesActions";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    },
    card: {
        height: '100%',
        width: '70%',
        display: "flex",
        justifyContent: 'space-between'
    },
    content: {
        display: 'flex',
        alignItems: 'center'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    },
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    button: {
        alignSelf: 'flex-end'
    }
}));

const FriendsList = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const friends = useSelector(state => state.friends.friends);

    useEffect(() => {
        dispatch(invitesRequest());
    }, [dispatch]);

    const rand = () => {
        return Math.round(Math.random() * 20) - 10;
    }

    const getModalStyle = () => {
        const top = 50 + rand();
        const left = 50 + rand();

        return {
            top: `${top}%`,
            left: `${left}%`,
            transform: `translate(-${top}%, -${left}%)`,
        };
    }

    const [modalStyle] = useState(getModalStyle);

    const deleteFriendHandler = id => {
        dispatch(inviteDeleteRequest(id));
    }

    return (
        <Grid item container direction="column" spacing={1} style={modalStyle} className={classes.paper}>
            <Grid item container direction="column" spacing={2}>
                {friends ? friends.friends.map(friend => {
                    return (
                        <Card className={classes.card} key={friend._id}>
                            <CardContent className={classes.content}>
                                <strong style={{marginLeft: '10px'}}>
                                    {friend.displayName}
                                </strong>
                            </CardContent>
                            <CardActions>
                                <IconButton color="primary" onClick={() => deleteFriendHandler(friend._id)}>
                                    <BackspaceIcon/>
                                </IconButton>
                            </CardActions>
                        </Card>
                    )
                }) : (
                    <Grid item>
                        <Typography variant="h6">You didn't invite friend yet</Typography>
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
};

export default FriendsList;