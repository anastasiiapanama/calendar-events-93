import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createEventRequest} from "../../store/actions/eventsActions";
import {Grid, Typography} from "@material-ui/core";
import EventForm from "../../components/EventForm/EventForm";

const NewEvents = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.events.createEventError);
    const loading = useSelector(state => state.events.createEventLoading);

    const onEventFormSubmit = (e, data) => {
        e.preventDefault();
        dispatch(createEventRequest(data));
    }

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">New event</Typography>
            </Grid>

            <Grid item xs>
                <EventForm
                    onSubmit={onEventFormSubmit}
                    loading={loading}
                    error={error}
                />
            </Grid>
        </Grid>
    );
};

export default NewEvents;