import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, makeStyles, Modal, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import EventItem from "./EventItem";
import {deleteEventRequest, eventsRequest} from "../../store/actions/eventsActions";
import {Link} from "react-router-dom";
import moment from "moment";
import InviteFriend from "../InviteFriend/InviteFriend";
import FriendsList from "../FriendsList/FriendsList";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Events = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.events.eventsLoading);
    const events = useSelector(state => state.events.events);
    const [open, setOpen] = useState(false);
    const [modalFriends, setModalFriend] = useState(false);

    useEffect(() => {
       dispatch(eventsRequest());
    }, [dispatch]);

    const onDeleteEvent = id => {
        dispatch(deleteEventRequest(id));
    }

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Events List</Typography>
                </Grid>


                <Grid item>
                    <Button color="primary" component={Link} to="/events/new">Add new event</Button>
                    <Button color="primary" onClick={() => setOpen(true)}>Invite friend</Button>
                    <Button color="primary" onClick={() => setModalFriend(true)}>View invited</Button>
                </Grid>
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : events.map(event => (
                    <EventItem
                        key={event._id}
                        id={event._id}
                        title={event.title}
                        dateEvent={moment(event.dateEvent).format('YYYY-DD-MM')}
                        datetime={event.datetime}
                        deleteEvent={() => onDeleteEvent(event._id)}
                    />
                ))}
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {events ? events.user.friends.map(friend => {
                    console.log(friend)
                    return (
                        <></>
                    )
                }) : null}
            </Grid>
            <Modal
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <InviteFriend />
            </Modal>
            <Modal
                open={modalFriends}
                onClose={() => setModalFriend(false)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <FriendsList />
            </Modal>
        </Grid>
    );
};

export default Events;