import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/core";
import {inviteFriendRequest} from "../../store/actions/invitesActions";

const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    button: {
        alignSelf: 'flex-end'
    }
}));

const InviteFriend = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.events.invited);
    const [email, setEmail] = useState('');

    const rand = () => {
        return Math.round(Math.random() * 20) - 10;
    }

    const getModalStyle = () => {
        const top = 50 + rand();
        const left = 50 + rand();

        return {
            top: `${top}%`,
            left: `${left}%`,
            transform: `translate(-${top}%, -${left}%)`,
        };
    }

    const [modalStyle] = useState(getModalStyle);

    const addFriendHandler = (event) => {
        event.preventDefault();

        dispatch(inviteFriendRequest({email: email}));
        setEmail('');
    }

    return (
        <form noValidate onSubmit={addFriendHandler} className={classes.paper} style={modalStyle}>
            <Grid container direction="column" spacing={2}>
                <FormElement
                    required
                    label="Email"
                    name="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />

                <Grid item xs className={classes.button}>
                    <ButtonWithProgress
                        type="submit"
                        color="primary"
                        variant="contained"
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </form>
    );
};

export default InviteFriend;