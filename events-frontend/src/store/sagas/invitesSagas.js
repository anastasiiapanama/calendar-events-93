import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {
    inviteDeleteRequest,
    inviteDeleteSuccess,
    inviteFriendFailure, inviteFriendRequest,
    inviteFriendSuccess,
    invitesFailure,
    invitesRequest,
    invitesSuccess
} from "../actions/invitesActions";
import {NotificationManager} from "react-notifications";
import {historyPush} from "../actions/historyActions";

export function* fetchInvites() {
    try {
        const response = yield axiosApi.get('/addFriend');

        yield put(invitesSuccess(response.data));
    } catch (e) {
        yield put(invitesFailure());
        NotificationManager.error('Could not fetch invited friend');
    }
}

export function* inviteFriend({payload: inviteData}) {
    try {
        yield axiosApi.post('/addFriend', inviteData);
        yield put(inviteFriendSuccess());
        yield put(historyPush('/'));

        NotificationManager.success('Invite created successfully');
    } catch (e) {
        yield put(inviteFriendFailure(e.response.data));
        NotificationManager.error('Could not invite friend');
    }
}

export function* deleteFriend({payload: id}) {
    try {
        yield axiosApi.delete('/addFriend/' + id);
        yield put(inviteDeleteSuccess(id));

    } catch (e) {
        NotificationManager.error('Could not delete friend');
    }
}

const invitesSagas = [
    takeEvery(invitesRequest, fetchInvites),
    takeEvery(inviteFriendRequest, inviteFriend),
    takeEvery(inviteDeleteRequest, deleteFriend)
]

export default invitesSagas;