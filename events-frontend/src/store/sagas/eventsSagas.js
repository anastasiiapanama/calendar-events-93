import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {
    createEventFailure, createEventRequest,
    createEventSuccess, deleteEventRequest, deleteEventSuccess,
    eventsFailure,
    eventsRequest,
    eventsSuccess
} from "../actions/eventsActions";
import {NotificationManager} from "react-notifications";
import {historyPush} from "../actions/historyActions";

export function* fetchEvents() {
    try {
        const response = yield axiosApi.get('/events');
        yield put(eventsSuccess(response.data));
    } catch (e) {
        yield put(eventsFailure());
        NotificationManager.error('Could not fetch events');
    }
}

export function* createEvent({payload: eventData}) {
    try {
        yield axiosApi.post('/events', eventData);
        yield put(createEventSuccess());
        yield put(historyPush('/'));
        NotificationManager.success('Event created successfully');
    } catch (e) {
        yield put(createEventFailure(e.response.data));
        NotificationManager.error('Could not create event');
    }
}

export function* deleteEvent({payload: id}) {
    try {
        yield axiosApi.delete('/events/' + id);
        yield put(deleteEventSuccess(id));
        yield put(fetchEvents());
    } catch (e) {
        NotificationManager.error('Could not delete event');
    }
}

const eventsSagas = [
    takeEvery(eventsRequest, fetchEvents),
    takeEvery(createEventRequest, createEvent),
    takeEvery(deleteEventRequest, deleteEvent)
];

export default eventsSagas;