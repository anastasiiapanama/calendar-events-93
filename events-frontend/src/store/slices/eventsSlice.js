import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    events: [],
    eventsLoading: false,
    createEventLoading: false,
    createEventError: null
};

const name = 'events';

const eventsSlice = createSlice({
    name,
    initialState,
    reducers: {
        eventsRequest: state => {
            state.eventsLoading = true;
        },
        eventsSuccess: (state, {payload: events}) => {
            state.eventsLoading = false;
            state.events = events;
        },
        eventsFailure: state => {
            state.eventsLoading = false;
        },
        createEventRequest: state => {
            state.createEventLoading = true;
        },
        createEventSuccess: state => {
            state.createEventLoading = false;
        },
        createEventFailure: (state, {payload: error}) => {
            state.createEventLoading = false;
            state.createEventError = error;
        },
        deleteEventRequest: state => {
            state.eventsLoading = true;
        },
        deleteEventSuccess: (state, {payload: id}) => {
            state.eventsLoading = false;
            state.events = state.events.filter(c => c.id !== id);
        },
        deleteEventFailure: state => {
            state.eventsLoading = false;
        }
    }
});

export default eventsSlice;