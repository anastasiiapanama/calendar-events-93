import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    friends: null,
    invitesLoading: false,
    inviteFriendLoading: false,
    inviteFriendError: null
};

const name = 'invites';

const invitesSlice = createSlice({
    name,
    initialState,
    reducers: {
        invitesRequest: state => {
            state.invitesLoading = true;
        },
        invitesSuccess: (state, {payload: friends}) => {
            state.invitesLoading = false;
            state.friends = friends;
        },
        invitesFailure: state => {
            state.invitesLoading = false;
        },
        inviteFriendRequest: state => {
            state.inviteFriendLoading = true;
        },
        inviteFriendSuccess: state => {
            state.inviteFriendLoading = false;
        },
        inviteFriendFailure: (state, {payload: error}) => {
            state.inviteFriendLoading = false;
            state.inviteFriendError = error;
        },
        inviteDeleteRequest: state => {
            state.invitesLoading = true;
        },
        inviteDeleteSuccess: (state, {payload: id}) => {
            state.invitesLoading = false;
            state.friends = state.friends.filter(c => c.id !== id);
        },
        inviteDeleteFailure: state => {
            state.invitesLoading = false;
        }
    }
});

export default invitesSlice;