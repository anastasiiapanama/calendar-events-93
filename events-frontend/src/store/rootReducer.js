import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import eventsSlice from "./slices/eventsSlice";
import invitesSlice from "./slices/invitesSlice";

const rootReducer = combineReducers({
    friends: invitesSlice.reducer,
    events: eventsSlice.reducer,
    users: usersSlice.reducer,
});

export default rootReducer;