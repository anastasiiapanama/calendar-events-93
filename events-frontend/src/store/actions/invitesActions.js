import invitesSlice from "../slices/invitesSlice";

export const {
    invitesRequest,
    invitesSuccess,
    invitesFailure,
    inviteFriendRequest,
    inviteFriendSuccess,
    inviteFriendFailure,
    inviteDeleteRequest,
    inviteDeleteSuccess,
    inviteDeleteFailure
} = invitesSlice.actions;