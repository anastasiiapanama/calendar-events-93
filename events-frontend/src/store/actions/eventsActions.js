import eventsSlice from "../slices/eventsSlice";

export const {
    eventsRequest,
    eventsSuccess,
    eventsFailure,
    createEventRequest,
    createEventSuccess,
    createEventFailure,
    deleteEventRequest,
    deleteEventSuccess
} = eventsSlice.actions