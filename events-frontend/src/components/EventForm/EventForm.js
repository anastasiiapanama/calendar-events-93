import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const EventForm = ({onSubmit, loading, error}) => {
    const [state, setState] = useState({
        dateEvent: '',
        title: '',
        datetime: ''
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form onSubmit={(e) => onSubmit(e, state)} noValidate>
            <Grid container direction="column" spacing={2}>
                <FormElement
                    required
                    label="Date format: YYYY-MM-DD"
                    name="dateEvent"
                    value={state.dateEvent}
                    onChange={inputChangeHandler}
                    error={getFieldError('dateEvent')}
                />

                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />

                <FormElement
                    label="Datetime"
                    name="datetime"
                    value={state.datetime}
                    onChange={inputChangeHandler}
                    error={getFieldError('datetime')}
                />

                <Grid item xs>
                    <ButtonWithProgress
                        type="submit"
                        color="primary"
                        variant="contained"
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </form>
    );
};

export default EventForm;