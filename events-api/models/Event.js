const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
    dateEvent: {
        type: Date,
        default: Date.now(),
        min: Date.now(),
        required: true
    },
    title: {
        type: String,
        required: true
    },
    datetime: {
        type: String,
        min: 0,
        required: true
    },
    user: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;