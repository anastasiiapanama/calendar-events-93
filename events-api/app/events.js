const express = require('express');
const Event = require('../models/Event');
const User = require('../models/User');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const user = req.user;
        const sharedUser = await User.find({friends: user});
        const ids = sharedUser.map(user => user._id);

        const currentData = (new Date()).toISOString();
        const events = await Event.find({user: {$in: [...ids, user._id]}, dateEvent: {$gt: currentData}});

        return res.send(events);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('user', 'admin'), async (req, res) => {
    try {
        const eventData = req.body;
        eventData.user = req.user._id;

        const event = new Event(eventData);
        req.user.events.push(event._id);
        await req.user.save()
        await event.save();

        return res.send(event);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const reqEvent = req.body;
        reqEvent.user = req.user._id;

        if (reqEvent.user) {
            const event = await Event.findOne({_id: req.params.id}).remove();
            res.send(event);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

router.put('/:id', auth, async (req, res) => {
    try {
        const reqEvent = req.body;
        reqEvent.user = req.user._id;

        if (reqEvent.user) {
            const event = await Event.findById({_id: req.params.id}).update(reqEvent);
            res.send(event);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;