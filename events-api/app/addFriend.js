const express = require('express');
const auth = require("../middleware/auth");
const User = require('../models/User');

const router = express.Router();

router.get('/', auth, async (req, res) => {
   try {
       const userLogin = await User.findOne({_id: req.user._id}).populate('friends');

       res.send(userLogin);
   } catch (e) {
       
   }
});

router.post('/', auth, async (req, res) => {
    try {
        const user = await User.findOne({email: req.body.email});

        if (user) {
            const userLogin = await User.findOne({_id: req.user._id});
            userLogin.friends.push(user._id);

            await userLogin.save();

            res.send({message: 'Success', user});
        }

        if (!user) {
            res.send({message: 'Email not found!'});
        }
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const reqFriend = req.body;
        reqFriend.user = req.user._id;

        if (reqFriend.user) {
            const friend = await User.findOne({_id: req.params.id}).remove();

            res.send(friend);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;

