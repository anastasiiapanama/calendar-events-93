const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Event = require('./models/Event');
const {nanoid} = require('nanoid');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [User1, Admin, User2] = await User.create({
        email: 'user@1',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User'
    }, {
        email: 'admin@1',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin'
    }, {
        email: 'vasya@1',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User2'
    });

    User1.friends = [Admin._id, User2._id];
    await User1.save();
    Admin.friends = [User1._id, User2._id];
    await Admin.save();
    User2.friends = [User1._id, Admin._id];
    await User2.save();

    const futureDate = new Date((new Date().getTime() + 30 * 60000)).toISOString();

    await Event.create({
        dateEvent: futureDate,
        title: 'Test event',
        datetime: '0.5',
        user: User1._id
    }, {
        dateEvent: (new Date()).toISOString(),
        title: 'Test event 2',
        datetime: '2',
        user: Admin._id
    }, {
        dateEvent: (new Date()).toISOString(),
        title: 'Test event 22',
        datetime: '2',
        user: Admin._id
    });

    await mongoose.connection.close();
};

run().catch(console.error);